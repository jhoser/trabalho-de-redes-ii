/*

Felipe Mesquita (flm17) e Jhoser Alaff (jasm16) 

*/

/*
	Este Ã© o segundo arquivo. Os comentarios aqui serÃ£o mais simples por falta de tempo. Coisas em comum com o servidor estao melhor explicadas lÃ¡.
*/

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <sys/time.h>
#include <curses.h>


// Estrutura da mensagem
typedef struct  str_mensagem {

    // Numero de sequencia da mensagem 
    unsigned int sequencia;
    
    // Bits de controle
    union {
        // Ler como inteiro por praticidade
        unsigned char _inteiro;
        
        // Ler como bitfield para economizar
        struct {
            unsigned char 
            quer_conectar : 1,
            fim_de_jogo : 1,
            numero : 1,
            reservado_para_uso_futuro: 5;
            // Mais coisas
        } _bits;
    } controle;
    unsigned int rgb;
    unsigned int r, g, b;

} msg_t;


// Calcula a diferenÃ§a de tempo em milisegundos
float timedifference_msec(struct timeval t0, struct timeval t1) {
    return (t1.tv_sec - t0.tv_sec) * 1000.0f + (t1.tv_usec - t0.tv_usec) / 1000.0f;
}

// Incia as coisinhas da biblioteca grafica
void inicializa_curses() {
    initscr();
    raw();
    nodelay(stdscr, TRUE);
    keypad(stdscr, TRUE);
    noecho();
    curs_set(0);
    start_color();
}


// Aqui acontece a magia
int main(int argc, char *argv[]) {

	// Um monte de coisas do socket. Mais detalhes  no servidor  
	int sockdescr;
	int numbytesrecv;
	struct sockaddr_in sa, so;
	struct hostent *hp;
	char buf [sizeof(msg_t)];
	msg_t* msg = (msg_t*)buf;
	char *host;
	char *dados;

	unsigned int i = sizeof(sa); 

	if(argc != 3) {
		fprintf(stderr,"Uso correto: <cliente> <nome-servidor> <porta>");
		exit(1);
	}

	fprintf(stderr,"Cliente iniciando. Erros serao informados.\n");

	host = argv[1];

	if((hp = gethostbyname(host)) == NULL){
		fprintf(stderr,"Nao consegui obter endereco IP do servidor.");
		exit(1);
	}
	bcopy((char *)hp->h_addr, (char *)&sa.sin_addr, hp->h_length);
	sa.sin_family = hp->h_addrtype;
	sa.sin_port = htons(atoi(argv[2]));
//	fprintf(stderr,"PORTA %u \n", sa.sin_port);

        struct timeval timeout;      
        timeout.tv_sec = 5;
        timeout.tv_usec = 0;
	if((sockdescr=socket(hp->h_addrtype, SOCK_DGRAM, 0)) < 0) {
		fprintf(stderr,"Nao consegui abrir o socket.");
		exit(1);
	}
        // Coloca o timeout no socket
        if (setsockopt (sockdescr, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout, sizeof(timeout)) < 0) {
                fprintf(stderr,"setsockopt failed.");
                exit (0);
        }
	// Aqui jÃ¡ ta pronto pra comunicar

	inicializa_curses();
	unsigned int  sequenciaEsperada = 1, nPacotesRecebidos = 0, nPacotesPerdidos = 0, nPacotesAtrasados = 0;

	// Avisa que quer conectar
	msg->controle._inteiro = 0;
	msg->sequencia = 0;
	msg->controle._bits.quer_conectar = 1;
	sendto(sockdescr, buf, sizeof(msg_t), 0, (struct sockaddr *) &sa, i);

	// Recebe a resposta
	if (recvfrom(sockdescr, buf, sizeof(msg_t), 0, (struct sockaddr *) &sa, &i) <= 0) {
                fprintf(stderr,"Erro ao conectar no servidor. Saindo.\n");
                exit(0);
        }

	fprintf(stderr,"Sou o cliente, consegui conectar no servidor. Tudo OK!\n");

    
        //inicia leitura de teclas
        char c = 0;
        short cc = 10;
        while (c != 'q') {//finaliza o jogo ao ser precionado 'q'


    	        // Recebe mensagem
if (recvfrom(sockdescr, buf, sizeof(msg_t), 0, (struct sockaddr *) &so, &i) <= 0) {
//if (recv(sockdescr, buf, sizeof(msg_t), 0) <= 0) {
			endwin();
			fprintf(stderr,"Erro ao receber mensagem. \nA ultima mensagem tinha numero %d. \nRecebi %d pacotes\n %d atrasados e\n %d perdidos.\nSaindo.", sequenciaEsperada, nPacotesRecebidos, nPacotesAtrasados, nPacotesPerdidos);
			// Fecha o socket
        		close(sockdescr);
			return 0;
		} 
                else {
                        if (msg->sequencia > sequenciaEsperada) {
                                nPacotesPerdidos++;
                                sequenciaEsperada = msg->sequencia+1;
                        }
                        else if (msg->sequencia < sequenciaEsperada) {
                                nPacotesPerdidos--;
                                nPacotesAtrasados++;
				//sequenciaEsperada++;
                        }
			else {
				sequenciaEsperada++;
			}

                } 
		fprintf(stderr,"Recebi a mensagem de sequencia %d.\n", msg->sequencia);
		//  Prepara os dados recebidos para mostrar na tela
		init_color(cc, msg->r, msg->g, msg->b);
    	        init_pair(cc+1, cc, cc);
	        if (cc > 15) {
		        for (int k = 0; k < 15; k++) {
			        for (int l = 0; l < k; l++) {
	    			        attron(COLOR_PAIR(cc+1-k+l));
				        mvprintw(5+k, 5+l*2, "###");
			        }
		        }
	        }
	        cc++;
                refresh();

                c = getch();
        }

        // Fecha a janela da ncurses
        endwin();

        // printa sÃ³ pra ficar bonito
        fprintf(stderr,"\n");
	fprintf (stderr,"Erro ao receber mensagem. \nA ultima mensagem tinha numero %d. \nRecebi %d pacotes\n %d atrasados  e\n %d perdidos.\nSaindo.", sequenciaEsperada, nPacotesRecebidos, nPacotesAtrasados, nPacotesPerdidos);

        // Fecha o socket
  	close(sockdescr);
	exit(0);
}
