// Servidor em UDP
//
// Felipe Mesquita - flm17   
// Jhoser Alaff - jasm16
//

#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <thread> 
#include <vector>
#include <iostream>

#include <time.h>
#include <sys/time.h>

using namespace std;


#define TAMFILA      5
#define MAXHOSTNAME 30
#define INTERVALO_GERAR_NUMERO 75 // Intervalo de tempo para gerar conteudo para transmitir
//#define MIN_NUMERO_GERADO 0
//#define MAX_NUMERO_GERADO 32000

#define RANGE 120 // Para o RGB
#define PASSO 3 // Anda de PASSO em PASSO no RANGE


typedef struct  str_mensagem {

    // Numero de sequencia da mensagem 
    unsigned int sequencia;
    
    // Bits de controle
    union {
        // Ler como inteiro por praticidade
        unsigned char _inteiro;
        
        // Ler como bitfield para economizar
        struct {
            unsigned char 
            quer_conectar : 1,
            fim_de_jogo : 1,
            numero : 1,
            reservado_para_uso_futuro: 5;
            // Mais coisas
        } _bits;
    } controle;

	// Manda tanto o rgb jÃ¡ deslocado quanto os componentes separados
    unsigned int rgb;
    unsigned int r, g, b;

} msg_t;



struct hostent *hp;
struct sockaddr_in sa, isa; 

// intervalo de tempo para mandar as mensagens
double intervalo;

// Numero de pessoas  conectadas
int nAssistindo = 0;

// Cor rgb atualmente sendo mandada para todo mundo
unsigned int numero_atual = 7;

// Uma lista para armazenar quem jÃ¡ entrou na rede. Na verdade, nÃ£o estÃ¡ sendo realmente utilizada,
// porque nÃ£o importa se alguÃ©m jÃ¡ entrou ou nÃ£o
typedef struct id_jogador_str {
    pid_t pid;
    uint32_t addr;
} id_jogador;

// Procura um ID jÃ¡ armazenado
id_jogador* procuraCliente (vector <id_jogador> lista, id_jogador* id_chegou) {
    vector<id_jogador>::iterator it;
    for (it = lista.begin(); it != lista.end(); ++it) {
        if (id_chegou->pid == it->pid && id_chegou->addr == it->addr) {
            return &(*it);
        }
    }
    return NULL;
}

// Uma funÃ§Ã£ozinha para calcular a diferenÃ§a entre dois tempos. Retorna em milisegundos
float timedifference_msec(struct timeval t0, struct timeval t1) {
    return (t1.tv_sec - t0.tv_sec) * 1000.0f + (t1.tv_usec - t0.tv_usec) / 1000.0f;
}

// Variaveis globais onde estÃ£o os dados atuais
// tem que ser global para que todas as threads possam ler
// SÃ³ a thread geradora de numeros escreve aqui
unsigned int cr, cg, cb;
unsigned int rgb(double hue) {
    int h = int(hue * 256 * 6);
    int x = h % 0x100;

    int r = 0, g = 0, b = 0;
    switch (h / 256)
    {
    case 0: r = 255; g = x;       break;
    case 1: g = 255; r = 255 - x; break;
    case 2: g = 255; b = x;       break;
    case 3: b = 255; g = 255 - x; break;
    case 4: b = 255; r = x;       break;
    case 5: r = 255; b = 255 - x; break;
    }
    cr = r;
    cg = g;
    cb = b;
    return r + (g << 8) + (b << 16);
}

// Essa Ã© a thread que gera um novo nÃºmero RGB
void geradorNumeros () {
    struct timeval start, stop;
    double dif;
    gettimeofday(&start, NULL);
    double i = 0;
    while(true) {
        gettimeofday(&stop, NULL);
        dif = timedifference_msec(start, stop);

        if (dif >= INTERVALO_GERAR_NUMERO) {
            gettimeofday(&start, NULL);
            //numero_atual = rand() % MAX_NUMERO_GERADO + MIN_NUMERO_GERADO;
        }

        numero_atual = rgb(i / RANGE);
	i+= PASSO;

    }

}

// Thread que Ã© chamada para cuidar de um novo cliente quando ele entra. Nome legacy
void jogo (unsigned int porta, int nJogador) {

    // Cria sues prÃ³prios dados para poder se comunicar com o cliente de forma independente da thread principal
    char buffer [sizeof(msg_t)];
    msg_t* msg = (msg_t*)buffer;

    int s, t;
    unsigned int size_isa;

    struct sockaddr_in sa, destino = isa;  /* sa: servidor, isa: cliente */
    size_isa = sizeof(destino); 

    // A porta de cada thread Ã© mandada pela thread principal apenas somando um, para que nennhuma porta se repita
    // Da problema se escolher uma porta muito proxima do limite final, mas nÃ£o deu tempo de arrumar
    sa.sin_port = htons(porta);
    bcopy ((char *) hp->h_addr, (char *) &sa.sin_addr, hp->h_length);
    sa.sin_family = hp->h_addrtype;   

    // Jeitinho de abrir o socket
	fprintf(stderr,"------- THREAD %d: Abrindo Socket.\n", nJogador);
    if ((s = socket(hp->h_addrtype,SOCK_DGRAM,0)) < 0) {
        fprintf(stderr, "------- THREAD %d: Nao consegui abrir o socket", nJogador);
        exit ( 1 );
    } 
fprintf(stderr,"------- THREAD %d: Conectando o Socket.\n", nJogador);
	connect(s, (struct sockaddr *)&destino, size_isa);
    // e de dar bind nele 
//    if (bind(s, (struct sockaddr *) &sa,sizeof(sa)) < 0) {
//        fprintf(stderr, "------- THREAD %d: Nao consegui fazer o bind", nJogador);
//        exit ( 1 );
//    }
    // Nesse ponto jÃ¡ tem o socket pronto no prÃ³prio IP e tau ----------------------

    struct timeval timeout;      
    timeout.tv_sec = 5;
    timeout.tv_usec = 0;
fprintf(stderr,"------- THREAD %d: Setando o timeout do socket.\n", nJogador);
    if (setsockopt (s, SOL_SOCKET, SO_SNDTIMEO, (char *)&timeout, sizeof(timeout)) < 0) {
        perror("setsockopt failed\n");
        
	exit (1);
   }


    // SeÃ§Ã£o das variÃ¡veis de controle
    unsigned int nPacotesAtrasados = 0;
    unsigned int nPacotesPerdidos = 0;

    //--------------------------------

    // Manda a nova porta para o jogador
    unsigned int minhaSequencia = 1;

    // usa essas estruturas para contar tempo
    struct timeval start, stop;
    double dif;
    gettimeofday(&start, NULL);

    fprintf(stderr,"------- THREAD %d: pronta, vou iniciar a rotina.\n", nJogador);

    while (true) {

        gettimeofday(&stop, NULL);
        dif = timedifference_msec(start, stop);

	// Se passou um determinado tempo desde a ultima vez que enviou uma mensagem, pode enviar novamente
        if (dif >= intervalo) {
            gettimeofday(&start, NULL);

            msg->controle._bits.numero = 1;
            msg->sequencia = minhaSequencia;
	    msg->rgb = numero_atual;
            msg->r = cr;
            msg->g = cg;
            msg->b = cb;
            //fprintf(stderr,"------- THREAD %d: Enviando a mensagem %d.\n", nJogador, minhaSequencia);
            if (sendto(s, buffer, sizeof(msg_t), 0, (struct sockaddr *) &destino, size_isa) <= 0) {
                fprintf(stderr,"------- THREAD %d: Perdi a conexao. Enviei %d pacotes. SAINDO.\n", nJogador, minhaSequencia);
            	return;
	    }
            fprintf(stderr,"------- THREAD %d: Enviei a mensagem %d com sucesso.\n", nJogador, minhaSequencia);
            minhaSequencia++;
            // Limpa o buffer
            memset(buffer, 0, sizeof(msg_t));
        }
    }
    
}


int main ( int argc, char *argv[] ) {

    fprintf(stderr,"Servidor Iniciando.\n");

    // Buffer que envia e recebe, e a estrutura da mensagem, que mascara o buffer
    char buffer [sizeof(msg_t)];
    msg_t* msg = (msg_t*)buffer;

    int s, t;
    unsigned int porta; // Numero da proxima porta a ser usada por uma thread
    unsigned int size_isa;
    char localhost [MAXHOSTNAME];

    if (argc == 1 || argc >= 4) {
        fprintf(stderr,"Uso correto: servidor <porta> [intervalo das mensagens]");
        exit(1);
    }

    // Pega o nome do host e entÃ£o procura o IP dele
fprintf(stderr,"Servidor adquirindo seu IP.\n");
    gethostname (localhost, MAXHOSTNAME);
    if ((hp = gethostbyname(localhost)) == NULL){
        fprintf(stderr,"Nao consegui meu proprio IP");
        exit (1);
    } 

    porta = atoi(argv[1]);
    sa.sin_port = htons(atoi(argv[1]));
    if (argc  == 3) {
        intervalo = atoi(argv[2]);
    }
    else {
        intervalo = INTERVALO_GERAR_NUMERO;
    }

    // INICIA A THREAD QUE GERA CONTEUDO
    bcopy ((char *) hp->h_addr, (char *) &sa.sin_addr, hp->h_length);
    sa.sin_family = hp->h_addrtype;   

fprintf(stderr,"Servidor vai criar o socket.\n");
    if ((s = socket(hp->h_addrtype,SOCK_DGRAM,0)) < 0) {
        fprintf(stderr, "Nao consegui abrir o socket" );
        exit ( 1 );
    } 

fprintf(stderr,"Servidor vai dar Bind no socket.\n");
    if (bind(s, (struct sockaddr *) &sa,sizeof(sa)) < 0) {
        fprintf(stderr, "Nao consegui fazer o bind" );
        exit ( 1 );
    }
    // Nesse ponto jÃ¡ tem o socket pronto no prÃ³prio IP e tau ----------------------
    thread (geradorNumeros).detach();

    vector <id_jogador> lista_jogadores; 

    fprintf(stderr,"Servidor pronto, vou comeÃ§ar a escutar.\n");

    size_isa = sizeof(isa); 
    // Loop onde recebe as mensagens para se conectarem
    while (true) {
        
        fprintf(stderr,"Vou bloquear.\n");

        recvfrom(s, buffer, sizeof(msg_t), 0, (struct sockaddr *) &isa, &size_isa);
        fprintf(stderr,"Sou o servidor, recebi uma mensagem. Host numero %d a se conectar. Vou adicionar aos espectadores.\n", nAssistindo+1);
        
        id_jogador_str id_chegou;
        id_chegou.pid = 0;
        id_chegou.addr = isa.sin_addr.s_addr;
        //if (procuraCliente(lista_jogadores, &id_chegou) != NULL) {
        //    fprintf(stderr,"AlguÃ©m jÃ¡ conhecido tentou se conectar.\n");
        //}
        //else {// Se nÃ£o estiver na lista de conhecidos, coloca ele lÃ¡
            id_jogador id_novo_jogador;
            id_novo_jogador.pid = 0;
            id_novo_jogador.addr = isa.sin_addr.s_addr;
            lista_jogadores.push_back(id_novo_jogador);

            // E agora separa uma thread para cuidar dele
            porta++;
fprintf(stderr,"Vou criar a thread para tratar dele.\n");
            thread (jogo, porta, nAssistindo).detach();
            nAssistindo++;
fprintf(stderr, "Thread criada.\n");

	//}

//    sendto(s, buffer, sizeof(msg_t), 0, (struct sockaddr *) &isa, size_isa);    
        memset(buffer, 0, sizeof(msg_t));
    }
}


